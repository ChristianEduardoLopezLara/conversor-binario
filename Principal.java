package org.christian;
import org.christian.LeerDatos.LeerDatos;
import org.christian.Convertidor.Convertidor;
/**
Clase Principal instancia a los metodos para llevar a cabo el proceso
*/
public class Principal{
	/**
	En la clase principal se instancia a la clase convertidor para llevar a cabo el proceso.
	*/
	/**
	Tambien se instancia a la clase leer datos para preguntar al usuario si quiere seguir en el proceso o no.
	*/
	public static void main (String args[]){
	Convertidor convertir=new Convertidor();
	String desicion;
	System.out.println("Convertidor");
	do{
		convertir.Convertir();
		System.out.println("Desea continuar");
		desicion=LeerDatos.Es();
	}while(desicion.equals("si"));
	}
}