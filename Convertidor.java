	package org.christian.Convertidor;
	import org.christian.LeerDatos.LeerDatos;
	/**
	@author Christian Lopez
	El metodo Convertir gestiona los numeros binarios para volverlos a decimal.
	*/
public class Convertidor{
	String numero;
	int numero1;
	String binario="";
	int num1;
	int cadena;
	boolean i;
	public void Convertir(){
	int base=1;
	int n=0;
	int n1=1;
	int binarioI=0;
	System.out.println("Ingrese el numero");
	numero=LeerDatos.Es();
	cadena=numero.length();
	i=Binario(numero);
	/**
	Segun el valor de Retorno del metodo Binario se proseguira con el proceso o no.
	*/
		if(i==true){
			n=cadena-1;
			n1=cadena;
		while(cadena>=1){
			num1=Integer.parseInt(numero.substring(n,n1));
			binarioI=binarioI+(num1*base);
			base=base*2;
			cadena=cadena-1;
			n--;
			n1--;
			}
		base=1;
		System.out.println(binarioI);
		}else{
		System.out.println("El numero no es Binario");
		}
	}
	/**
	@param El metodo Binario recibe como parametro un String .
	En el metodo se identifica a un entero como binario o no.
	*/
	public boolean Binario(String numero){
		try{
			for(char n : numero.toCharArray())
				if(Integer.parseInt(Character.toString(n)) > 1)
					return false;
		}catch(NumberFormatException numex){
			return false;
		}
		return true;
		/**
		@return Retorna un valor buleano para seguir con el proceso o no.
		*/
	}
}